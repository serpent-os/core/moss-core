# moss-core

A very small module to provide some shared utility APIs for the moss
suite of tooling, as well as some C interfaces and encoding routines
for projects like [moss-db](https://gitlab.com/serpent-os/core/moss-db.

### License

Copyright &copy; 2020-2022 [Serpent OS](https://serpentos.com) Developers.

Available under the terms of the [Zlib](https://opensource.org/licenses/Zlib) license.
